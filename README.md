## 接口文档
> + isOn-----------------------------------灯的当前开关状态0是关1是开
> + timerOnStartTimeToSecond----灯定时开时间段的开始时间转换成秒:小时×3600+分钟×60
> + timerOnEndTimeToSecond-----灯定时开时间段的结束时间转换成秒
> + timerOffStartTimeToSecond----灯定时关时间段的开始时间转换成秒
> + timerOffEndTimeToSecond-----灯定时关时间段的结束时间转换成秒
> + timerOnStatus----------------------灯的定时开状态
> + timerOffStatus----------------------灯的定时关状态
> + lightValue----------------------------灯的光照强度

### 以灯一为例
> 灯一打开
>> http://10.192.168.132/led_1/on_off?isOn=1
> 
> 灯一关闭
>> http://10.192.168.132/led_1/on_off?isOn=0
> 
> 灯一在 20:00 到 20:30 定时开
>> http://10.192.168.132/led_1/confirm_on?timerOnStatus=true&timerOnStartTimeSecond=72000&timerOnEndTimeSecond=73800
> 
> 灯一在 20:00 到 20:30 定时关
>>http://10.192.168.132/led_1/confirm_off?timerOffStatus=true&timerOffStartTimeSecond=72000&timerOffEndTimeSecond=73800
>
> 灯一取消定时开
>> http://10.192.168.132/led_1/cancel_on?timerOnStatus=false
> 
> 灯一取消定时关
>> http://10.192.168.132/led_1/cancel_off?timerOffStatus=false
> 
> 灯一 mqttClient接收光照信息,无用户名和密码,订阅主题(topic)是: lightValue_1,服务端将灯一8266发来的光照信息发布到
> lightValue_1主题即可，mqtt服务器端口url在设置页面填写,有示例样式，ws://+ip+端口