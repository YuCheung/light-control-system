import { createStore } from 'vuex'
import {ref} from "vue";
import createPersistedState from "vuex-persistedstate";
export default createStore({
    state: {
        lights: [
            {
                id: 1,//灯的id号
                name: '灯1',//灯的名称
                isOn: 0,//灯的当前开关状态0是关1是开
                isTimerOnOff: null,//灯的定时状态
                timerTime: null,//灯在选择框里面的定时时间timerTime[0]是开始时间，timerTimer[1]是结束时间，格式：小时:分钟(HH:mm)
                timerOnStartTime: null,//灯定时开时间段的开始时间
                timerOnEndTime: null,//灯定时开时间段的结束时间
                timerOffStartTime: null,//灯定时关时间段的开始时间
                timerOffEndTime: null,//灯定时关时间段的结束时间
                timerOnStartTimeToSecond: null,//灯定时开时间段的开始时间转换成秒:小时*3600+分钟*60
                timerOnEndTimeToSecond: null,//灯定时开时间段的结束时间转换成秒
                timerOffStartTimeToSecond: null,//灯定时关时间段的开始时间转换成秒
                timerOffEndTimeToSecond: null,//灯定时关时间段的结束时间转换成秒
                timerOnStatus: false,//灯的定时开状态
                timerOffStatus: false,//灯的定时关状态
                showTimerDialog: false,
                showLightValueDialog: false,
                timerOnStartInterval: null,//灯定时开开始时刻计时器
                timerOnEndInterval: null,//灯定时开结束时刻计时器
                timerOffStartInterval: null,//灯定时开开始时刻计时器
                timerOffEndInterval: null,//灯定时开结束时刻计时器
                timerOnDate: null,//灯定时开当天的日期
                timerOffDate: null,//灯定时关当天的日期
                api: 'http://10.192.168.132',//灯控制路由
                lightValue: null,//灯的光照强度
                lightValueThreshold: null,
                mqttClient:null,//灯的mqtt客户端
                mqttServerUrl:'ws://10.192.168.132:8083',//灯的mqtt服务服务端点，末尾的"/mqtt"已在后续程序中添加
            },
            {
                id: 2,
                name: '灯2',
                isOn: 0,
                isTimerOnOff: null,
                timerTime: null,
                timerOnStartTime: null,
                timerOnEndTime: null,
                timerOffStartTime: null,
                timerOffEndTime: null,
                timerOnStartTimeToSecond: null,
                timerOnEndTimeToSecond: null,
                timerOffStartTimeToSecond: null,
                timerOffEndTimeToSecond: null,
                timerOnStatus: false,//灯的定时开状态
                timerOffStatus: false,//灯的定时关状态
                showTimerDialog: false,
                showLightValueDialog: false,
                timerOnStartInterval: null,//灯定时开开始时刻计时器
                timerOnEndInterval: null,//灯定时开结束时刻计时器
                timerOffStartInterval: null,//灯定时开开始时刻计时器
                timerOffEndInterval: null,//灯定时开结束时刻计时器
                timerOnDate: null,
                timerOffDate: null,
                api: 'http://10.192.168.132',
                lightValue: 0,//灯的光照强度
                lightValueThreshold: 0,
                mqttClient:null,
                mqttServerUrl:'ws://10.192.168.132:8083',
            },
            {
                id: 3,
                name: '灯3',
                isOn: 0,
                isTimerOnOff: null,
                timerTime: null,
                timerOnStartTime: null,
                timerOnEndTime: null,
                timerOffStartTime: null,
                timerOffEndTime: null,
                timerOnStartTimeToSecond: null,
                timerOnEndTimeToSecond: null,
                timerOffStartTimeToSecond: null,
                timerOffEndTimeToSecond: null,
                timerOnStatus: false,//灯的定时开状态
                timerOffStatus: false,//灯的定时关状态
                showTimerDialog: false,
                showLightValueDialog: false,
                timerOnStartInterval: null,//灯定时开开始时刻计时器
                timerOnEndInterval: null,//灯定时开结束时刻计时器
                timerOffStartInterval: null,//灯定时开开始时刻计时器
                timerOffEndInterval: null,//灯定时开结束时刻计时器
                timerOnDate: null,
                timerOffDate: null,
                api: 'http://10.192.168.132',
                lightValue: null,//灯的光照强度
                lightValueThreshold: null,
                mqttClient:null,
                mqttServerUrl:'ws://10.192.168.132:8083',
            },
            {
                id: 4,
                name: '灯4',
                isOn: 0,
                isTimerOnOff: null,
                timerTime: null,
                timerOnStartTime: null,
                timerOnEndTime: null,
                timerOffStartTime: null,
                timerOffEndTime: null,
                timerOnStartTimeToSecond: null,
                timerOnEndTimeToSecond: null,
                timerOffStartTimeToSecond: null,
                timerOffEndTimeToSecond: null,
                timerOnStatus: false,//灯的定时开状态
                timerOffStatus: false,//灯的定时关状态
                showTimerDialog: false,
                showLightValueDialog: false,
                timerOnStartInterval: null,//灯定时开开始时刻计时器
                timerOnEndInterval: null,//灯定时开结束时刻计时器
                timerOffStartInterval: null,//灯定时开开始时刻计时器
                timerOffEndInterval: null,//灯定时开结束时刻计时器
                timerOnDate: null,
                timerOffDate: null,
                api: 'http://10.192.168.132',
                lightValue: null,//灯的光照强度
                lightValueThreshold: null,
                mqttClient:null,
                mqttServerUrl:'ws://10.192.168.132:8083',
            },
        ],
    },
    getters: {
        getLights: state => state.lights,
    },
    mutations: {
        setLights(state,newLights){
            state.lights = newLights;
        },
    },
    actions: {
    },
    modules: {
    },
    plugins: [
        createPersistedState({
            // 存储方式：localStorage、sessionStorage、cookies
            storage: window.cookies,
            // 存储的 key 的key值
            key: "store",
            render(state) {
                // 要存储的数据：本项目采用es6扩展运算符的方式存储了state中所有的数据
                return { ...state };
            }
        })
    ]
})


